> LIBRARY VERSIONS USED 

```
"react": "16.6.3",
"react-native": "0.58.6",
"react-native-navigation": "^2.13.1",
"react-native-app-intro-slider": "^1.0.1",
"react-native-splash-screen": "^3.2.0",
"react-native-super-grid": "^3.0.3",
"react-redux": "^6.0.1",
"redux": "^4.0.1",
"axios": "^0.18.0",
"react-native-image-picker": "^0.28.0",
```


**Android Release commands**

`cd android`

`./gradlew app:assembleRelease`

`for keystore file and access contact the developer alan@iw.digital`